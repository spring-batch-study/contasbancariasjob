package com.springbatch.contasbancarias.writer;

import com.springbatch.contasbancarias.dominio.Conta;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.batch.item.support.builder.CompositeItemWriterBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Classe que inclui os dois escritores no batch (jdbc e arquivo)
 * Deve ser registrado no step como CompositeItemWriter.
 */
@Configuration
public class CompositeContaWriterConfig {

    @Bean
    public CompositeItemWriter<Conta> compositeContaWriter(
            final FlatFileItemWriter<Conta> flatFileItemWriter,
            final JdbcBatchItemWriter<Conta> jdbcBatchItemWriter) {
        return new CompositeItemWriterBuilder<Conta>()
                .delegates(flatFileItemWriter, jdbcBatchItemWriter)
                .build();
    }

}
